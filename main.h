// Internal methods

/**
 * Called to clean up handles on exit
 */
void _atexit_cleanup();

/**
 * Catch terminating signal handlers to exit cleanly
 */
void _terminating_signal_handler();

/**
 * Catch SIGCHLD to avoid creating zombie processes
 */
void _child_exit_signal_handler();