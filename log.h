#pragma once

#ifdef DEBUG
    #define debug_log(msg) _debug_log(msg)

    /**
     * Logs a debug message to the console
     * 
     * @param {message} The debug message that should be logged
     */
    void _debug_log(char *message);
#else
    #define debug_log(msg)
#endif

/**
 * Logs a message to the console
 * 
 * @param {message} The message that should be logged
 */
void info_log(char *message);

/**
 * Logs an error message to the console (stderr)
 * 
 * @param {message} The error message that should be logged
 */
void error_log(char *message);