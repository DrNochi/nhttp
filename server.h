#pragma once

#include <stdint.h>

/**
 * The HTTP request methods the server knows about
 * 
 * @value {UNKNOWN} The server did not recognize the request method used by the client
 * @value {GET} The client is trying to retrieve a file given by the url field of the http_request
 */
enum http_request_method
{
    UNKNOWN,
    GET
};

/**
 * Abstract representation of a HTTP request
 * 
 * @field {method} The request method used by the client
 * @field {url} The url of the resource the client wants to retrieve
 * @field {http_major_version} The major HTTP version used by the client
 * @field {http_minor_version} The minor HTTP version used by the client
 */
struct http_request
{
    enum http_request_method method;
    char url[1024];
    int http_major_version;
    int http_minor_version;
};

// Public methods

/**
 * "Starts the server" and begins serving requests
 * Called by main() to "start the server"
 * 
 * @param {port} Port the server should listen on
 * @param {webroot} The webroot directory the server should use to serve files
 */
void run_server(uint16_t port, char *webroot);

/**
 * Closes all remaining open handles of the server
 * Called by _atexit_cleanup() to close all the handles before the whole server or child process exits
 */
void stop_server();

/**
 * Logs a fatal error and stops the server / child process
 * This sends "500 Internal Server Error" error to the client (if client_socket is open)
 * 
 * @param {message} The message that should be logged using error_log()
 */
void error(char *message);

// Internal methods

/**
 * Does basic setup needed so that the server can start serving request (opens connection_socket)
 * 
 * @param {message} The port number to which the socket should be bound to
 */
void _setup_server(uint16_t port);

/**
 * Reads a line of data (until \r\n) from a socket and saves it to a buffer
 * borrowed from Tiny HTTPd https://sourceforge.net/projects/tinyhttpd/?source=typ_redirect
 * 
 * @param {socket} The socket to read the data from
 * @param {buffer} The buffer to write the data to
 * @param {buffer_size} The size of the buffer (to avoid buffer overflows)
 */
int _get_request_line(int socket, char *buffer, int buffer_size);

/**
 * Recieves a HTTP request on a socket and writes the important data into a http_request struct
 * 
 * @param {socket} The socket the request is received on
 * @param out {request_out} The pointer to the http_request struct
 */
void _receive_http_request(int socket, struct http_request *request_out);

/**
 * Responds to a HTTP request over a socket
 * 
 * @param {socket} The socket that should be used to send the response
 * @param {request} The request the server responds to
 * @param {webroot} The directory the server should serve files from
 */
void _respond_to_http_request(int socket, struct http_request *request, char *webroot);

/**
 * Sends the contents of a file over a socket
 * 
 * @param {socket} The socket that should be used to send the data
 * @param {filename} The file that the server should send
 */
void _send_file(int socket, char *filename);

/**
 * Sends a HTTP response header over a socket
 * 
 * @param {socket} The socket over which the header will be sent
 * @param {status} The status code used in the header
 * @param {header_attributes} The header attributes that should be sent. If they are not empty, they must end with '\r\n'
 */
void _send_http_response_header(int socket, char *status, char *header_attributes);