#include "server.h"

#include <stdlib.h>
#include <stdio.h>

#include <string.h>

#include <unistd.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <arpa/inet.h>

#include "log.h"

/*
 * Global variables
 * ------------------------------------
 * Needed so that the stop_server() method can close
 * all open handles! (used on exit and in case of an error/crash)
 */

int connection_socket; // server listen()'s and accept()'s new connections on this socket
int client_socket; // on this socket the actual communication with the client takes place

int requested_file; // handle to the file requested by the client

// "Starts the server" and begins serving requests
void run_server(uint16_t port, char *webroot)
{
    _setup_server(port);

    debug_log("Start listening on connection_socket");

    if (listen(connection_socket, 8))
        error("Could not start to listen on socket");

    debug_log("Entering accept() loop for connection_socket");

    while (1)
    {
        debug_log("Waiting for new connection to connection_socket");

        struct sockaddr client_socket_addr;
        socklen_t client_socket_addr_size;
        if ((client_socket = accept(connection_socket, &client_socket_addr, &client_socket_addr_size)) == -1)
            error("Could not accept connection");

        debug_log("Accepted connection to connection_socket on client_socket");

        debug_log("Forking to parallely process the request");

        switch (fork())
        {
        case -1:
            error("Could not fork a new child process to handle the connection");
            break;

        case 0: // Child process
        {
            struct http_request request;
            memset(&request, 0, sizeof(request));
            _receive_http_request(client_socket, &request);

            // Log the request
            char request_log_string[1024];
            snprintf(request_log_string, sizeof(request_log_string), "Request from %s: %.1008s",
                     inet_ntoa(((struct sockaddr_in *)&client_socket_addr)->sin_addr),
                     request.url);
            info_log(request_log_string);

            _respond_to_http_request(client_socket, &request, webroot);

            debug_log("[CHILD] Closing connection on client_socket");
            if (close(client_socket) == -1)
                error("[CHILD] Could not close socket");
            client_socket = 0;

            debug_log("[CHILD] End of request handling, child may exit now");
            exit(0);
            break;
        }

        default: // Parent process
            debug_log("[PARENT] Closing connection on client_socket");

            if (close(client_socket) == -1)
                error("[PARENT] Could not close socket");
            client_socket = 0;
            break;
        }
    }
}

// Closes all remaining open handles of the server
void stop_server()
{
    if (requested_file != 0)
        if (close(requested_file) == -1)
            error_log("Could not close file");

    if (client_socket != 0)
        if (close(client_socket) == -1)
            error_log("Could not close socket");

    if (connection_socket != 0)
        if (close(connection_socket) == -1)
            error_log("Could not close socket");
}

// Logs a fatal error and stops the server / child process
// This sends "500 Internal Server Error" error to the client (if client_socket is open)
void error(char *message)
{
    error_log(message);

    if (client_socket != 0)
        _send_http_response_header(client_socket, "500 Internal Server Error", "");

    exit(1);
}

// Does basic setup needed so that the server can start serving request (opens connection_socket)
void _setup_server(uint16_t port)
{
    debug_log("Constructing socketaddr_in connection_socket_addr");

    struct sockaddr_in connection_socket_addr;
    memset(&connection_socket_addr, 0, sizeof(connection_socket_addr));
    connection_socket_addr.sin_port = htons(port); // Convert to network byte order

    debug_log("Opening connection_socket");

    if ((connection_socket = socket(AF_INET, SOCK_STREAM, 0)) == -1)
        error("Could not create socket");

    debug_log("Binding connection_socket to connection_socket_addr");

    if (bind(connection_socket, (struct sockaddr *)&connection_socket_addr, sizeof(connection_socket_addr)) == -1)
        error("Could not bind socket to port");
}

// Reads a line of data (until \r\n) from a socket and saves it to a buffer
// get_line was borrowed from Tiny HTTPd under GPLv2
// https://sourceforge.net/projects/tinyhttpd/?source=typ_redirect
int _get_request_line(int sock, char *buf, int size)
{
    int i = 0;
    char c = '\0';
    int n;

    while ((i < size - 1) && (c != '\n'))
    {
        n = recv(sock, &c, 1, 0);
        /* DEBUG printf("%02X\n", c); */
        if (n > 0)
        {
            if (c == '\r')
            {
                n = recv(sock, &c, 1, MSG_PEEK);
                /* DEBUG printf("%02X\n", c); */
                if ((n > 0) && (c == '\n'))
                    recv(sock, &c, 1, 0);
                else
                    c = '\n';
            }

            buf[i] = c;
            i++;
        }
        else
            c = '\n';
    }

    buf[i] = '\0';

    return i;
}

// Recieves a HTTP request on a socket and writes the important data into a http_request struct
void _receive_http_request(int socket, struct http_request *request_out)
{
    debug_log("[CHILD] Recieving request");

    char buffer[1024];
    int len = _get_request_line(socket, buffer, sizeof(buffer));

    if (len > 0 && strcmp("\n", buffer))
    {
        /* Parse first line of the HTTP header */

        // Split request method
        char *token = strtok(buffer, " ");
        if (!strcmp(token, "GET"))
            request_out->method = GET;

        // Get URL from request
        token = strtok(NULL, " ");
        strncpy(request_out->url, token, sizeof(request_out->url));

        // Read HTTP version
        token = strtok(NULL, " ");
        sscanf(token, "HTTP/%u.%u", &request_out->http_major_version, &request_out->http_minor_version);

        // Read rest of request
        while (1)
        {
            len = _get_request_line(socket, buffer, sizeof(buffer));
            if (len > 0 && strcmp("\n", buffer))
                ; // Future extension: Parse header attributes
            else
                break;
        }
    }
}

// Responds to a HTTP request over a socket
void _respond_to_http_request(int socket, struct http_request *request, char *webroot)
{
    debug_log("[CHILD] Processing request & sending response");

    switch (request->method)
    {
    case GET:
    {
        // Create path of requested file from url and webroot
        char filename[1024];
        strncpy(filename, webroot, sizeof(filename));
        strncat(filename, request->url, sizeof(filename));

        _send_file(socket, filename);
        break;
    }

    default:
        _send_http_response_header(socket, "501 Not Implemented", "");
        break;
    }

    debug_log("[CHILD] Sent response to client");
}

// Sends the contents of a file over a socket
void _send_file(int socket, char *filename)
{
    debug_log("[CHILD] Opening requested file");

    // Test access rights and try opening the file
    if (access(filename, R_OK) || (requested_file = open(filename, O_RDONLY)) == -1)
    {
        debug_log("[CHILD] Requested file not found");
        _send_http_response_header(socket, "404 Not Found", "");
        return;
    }

    _send_http_response_header(socket, "200 OK", "Content-Type: text/html\r\n");

    // Read max. 1kB from the file and send, then repeat
    int len;
    char buffer[1024];
    while ((len = read(requested_file, buffer, sizeof(buffer))) > 0)
    {
        if (send(socket, buffer, len, 0) == -1)
            error("[CHILD] Could not send response");
    }

    debug_log("[CHILD] Closing requested file");

    if (close(requested_file) == -1)
        error_log("Could not close file");
    requested_file = 0;
}

// Sends a HTTP response header over a socket
void _send_http_response_header(int socket, char *status, char *header_attributes) // If header_attributes are not empty, they must end with '\r\n'
{
    char buffer[1024];
    snprintf(buffer, sizeof(buffer), "HTTP/1.0 %s\r\nServer: Webserver Team 11\r\n%s\r\n", status, header_attributes);
    if (send(socket, buffer, strlen(buffer), 0) == -1)
        error("[CHILD] Could not send response");
}

// const size_t request_buf_size_step = 1024;
// const size_t request_buf_size_max = 1024 * 16;

// char *request_buf;
// size_t request_buf_size;

// void _read_request_line() {
//     size_t request_buf_size = request_buf_size_step;
//     if ((request_buf = malloc(request_buf_size)) == 0)
//         error("[CHILD] Could not allocate enough memory");

//     ssize_t request_len = 0;
//     do
//     {
//         ssize_t read_request_len;
//         if ((read_request_len = recv(client_socket, request_buf + request_len, request_buf_size_step, 0)) == -1)
//             error("[CHILD] Could not recieve request");
//         request_len += read_request_len;

//         // printf("%d %d\n", read_request_len, request_len);

//         if ((size_t)request_len == request_buf_size)
//             if ((request_buf_size += request_buf_size_step) <= request_buf_size_max)
//             {
//                 if ((request_buf = realloc(request_buf, request_buf_size)) == NULL)
//                     error("[CHILD] Could not allocate enough memory");
//             }
//             else
//             {
//                 error("[CHILD] Request too big");
//             }
//         else
//         {
//             request_buf[request_len] = '\0';
//             break;
//         }
//     } while (1);

//     info_log("======= [CHILD] REQUEST =======");
//     info_log(request_buf);

//     free(request_buf);
//     request_buf = 0;
// }