#include "main.h"

#include <stdlib.h>

#include <signal.h>
#include <errno.h>
#include <limits.h>

#include <unistd.h>

#include <sys/wait.h>

#include "log.h"
#include "server.h"

int main(int argc, char *argv[])
{
    if (argc != 3)
        error("Wrong usage -> server PORT WEBROOT");

    debug_log("Registring exit handlers");

    // Exit handler to clean up handles on exit
    if (atexit(_atexit_cleanup))
        error("Could not register atexit() handler _atexit_cleanup()");

    debug_log("Registring signal handlers");

    // Catch terminating signal handlers (HUP, INT, TERM) to exit cleanly
    if (signal(SIGHUP, _terminating_signal_handler) == SIG_ERR)
        error("Could not register SIGHUP handler _terminating_signal_handler()");
    if (signal(SIGINT, _terminating_signal_handler) == SIG_ERR)
        error("Could not register SIGINT handler _terminating_signal_handler()");
    if (signal(SIGTERM, _terminating_signal_handler) == SIG_ERR)
        error("Could not register SIGTERM handler _terminating_signal_handler()");
    
    // Catch SIGCHLD to avoid creating zombie processes
    if (signal(SIGCHLD, _child_exit_signal_handler) == SIG_ERR)
        error("Could not register SIGCHLD handler _child_exit_signal_handler()");

    debug_log("Reading port for connection_socket_addr from cmdline args");

    char *port_arg_str_rest;
    unsigned long port_arg = strtoul(argv[1], &port_arg_str_rest, 0);
    if (*port_arg_str_rest != '\0' || (errno == ERANGE && port_arg == ULONG_MAX))
        error("Invalid port");

    // "Start the server"
    run_server(port_arg, argv[2]);

    return 0;
}

// Called to clean up handles on exit
void _atexit_cleanup()
{
    debug_log("Cleaning up handles");

    stop_server();

    debug_log("Exiting");
}

// Catch terminating signal handlers to exit cleanly
void _terminating_signal_handler()
{
    debug_log("Terminating by signal");
    exit(0);
}

// Catch SIGCHLD to avoid creating zombie processes
void _child_exit_signal_handler()
{
    debug_log("Waiting for child processes to clean up zombies");

    if (wait(NULL) == -1)
        error("Could not wait for child");

    debug_log("Waiting for child processes finished");
}