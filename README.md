# nHTTP

## Usage

```
server <PORT> <WEBROOT>
```

**PORT**: The port the webserver will listen on \
**WEBROOT**: The directory the webserver is serving files from

Example: `$ ./server 8080 /srv/nHTTP`

## Building

```
$ make clean
$ make server[_debug]
```

The default build target is `server_debug`

---

*A small webserver*