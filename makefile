object_files = main.o log.o server.o

server_debug: debug_option = -DDEBUG
server_debug: server

server: $(object_files)
	gcc -o $@ $^

%.o: %.c
	gcc -c -g $(debug_option) -Wall -Wextra -o $@ $<

clean:
	rm -f server $(object_files)