#include "log.h"

#include <stdio.h>

#include <errno.h>
#include <string.h>

#include <unistd.h>

#ifdef DEBUG
    // Logs a debug message to the console
    void _debug_log(char *message)
    {
        printf("\x1b[90mDEBUG [%d] : %s\x1b[m\n", getpid(), message);
    }
#endif

// Logs a message to the console
void info_log(char *message)
{
    printf("INFO [%d] : %s\n", getpid(), message);
}

// Logs an error message to the console (stderr)
void error_log(char *message)
{
    fprintf(stderr, "\n");

#ifdef DEBUG
    perror("\x1b[31mDEBUG [perror] ");
    fprintf(stderr, "DEBUG [strerror] : %s\x1b[m\n", strerror(errno));
#endif
    fprintf(stderr, "\x1b[30;41mERROR [%d] : %s\x1b[m\n", getpid(), message);

    fprintf(stderr, "\n");
}